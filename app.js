const express = require('express');
const app = express();

app.set('view engine','ejs');

app.use(express.static('public'));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//routes
app.get('/', (req,res) => {
  res.render('index');
})

/**
 * default port
 * Can custom base on hosting
 */
const port = 3030; 
server = app.listen(port);
const io = require('socket.io')(server);

io.on('connection', socket => {
  /**
   * Do not change channel 'verify'
   */
  socket.on('verify', (data) => {
    const channel = data.session;
    const channelRes = data.channel;
    //Send data to dApp
    io.emit(channel,data);

    //Response for SiriusID Application
    io.emit(channelRes,true);
  })
  
})
